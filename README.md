# README #


## Opis działania ##

Aplikacja ta ma za zadanie pobrać od użytkownika wyrażenie bez niewiadomych i zwrócić jego wartość. Dopuszczone operacje: {**+ - * / % () S(a) P(a, b)  **}, gdzie **S(a)** (ang. *square root*) oznacza pierwiastek kwadratowy z liczby a, natomiast **P(a, b)** (ang. *pow*) oznacza potęgę liczby **a** o wykładniku **b**.

Dodatkową funkcją będzie wyświetlanie drzewa zbudowanego na podstawie wprowadzonego wyrażenia (z lub bez uwzględnienia kolejności działań). 

### Kolejność działań ###

Operacje o różnej wadze będą wykonywane według podanej kolejności:

1. ()
2. / * % P(a, b) S(a)
3. \- +

Operacje o równej wadze będą wykonywane od lewej do prawej, np. 5/6/7 --> (5/6)/7

Aby wymusić ułamek należy umieścić go w nawiasach (np. przy potęgowaniu z niecałkowitym wykładnikiem --> 5^(3/4)).

## Algorytm ##

### Kolejność wykonywanych operacji: ###
1. Pobranie wyrażenie (w notacji infiksowej)
2. Sprawdzenie składni (operatory bez wymaganych argumentów, poprawne umiejscowienie nawiasów itd.)
3. Budowanie drzewa binarnego
4. Przekształcanie drzewa na podstawie zadanej kolejności działań poprzez rotację 
4. Rozwiązywanie drzewa
5. Wypisanie rozwiązania

### Sposób budowania drzewa ###

Każdy węzeł reprezentuje operacją algebraiczną na dwóch argumentach (gałęziach). Wyjątek stanowią "liście" - bezdzietne węzły, które przechowują jedynie wartość liczbową. 

We wszystkich węzłach znajdują się:

* wskaźniki na odchodzące niżej gałęzie
* zmienna zawierająca informację o operacji algebraicznej
* zmienna z wynikiem powyższej operacji na obu swoich gałęziach

### Rozwiązywanie drzewa ###

Podczas budowania drzewa do zmiennej przechowującej operację algebraiczną wpisuje się:
* w liściach: znak '='
* w nie-liściach: znak odpowiadający danej operacji
Natomiast do zmiennych z wartościami:
* w liściach: daną wartość
* w nie-liściach: NULL

Do przechodzenia drzewa użyte zostało przejście wzdłużne (ang. *pre-order traversal*), polegające na tym, że zaczynając od odwiedzenia korzenia rekurencyjnie przechodzi się do jego lewej gałęzi, a później do prawej.