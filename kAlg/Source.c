#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>
#include "tree.h"
#include "equation.h"

void InitMessage(void);

int main() {
	/*** Struktury i zmienne ***/
	char* equation;
	Node* root = NULL;
	int ParsingResult;
	double result;
	int curCharPos = 0;

	/*** Wiadomo�� powitalna ***/
	InitMessage();

	while (1) {
		equation = GetEquation(); // Pobieranie r�wnania
		equation = DelWhiteCharsAndUpperCase(equation);
		ParsingResult = ParseEquation(equation);
		if (!ParsingResult) { // Sprawdzanie sk�adni -> niedozwolone znaki, brak nawias�w, samotne operatory
			printf("Parsowanie zako�czone POMY�LNIE\n\n");
			break;
		}
		else
			printf("B��d sk�adni nr. %d - spr�buj jeszcze raz: ", ParsingResult);
	}

	/*** Budowanie drzewa ***/
	root = BuildTree(equation, &curCharPos);
	DisplayTree(root);

	/*** Przebudowa drzewa ***/
	ReBuildTree(&root);
	DisplayTree(root);
	
	/*** Rozwi�zywanie drzewa ***/
	result = SolveTree(root);
	printf("Wynik to: %g", result);
	
	/*** Zwalnienie pami�ci ***/
	free(equation);

	getchar();
	return 0;
}

void InitMessage(void) { //Zrobi� jakie� �adne powitanie
	setlocale(LC_ALL, "C");
	setlocale(LC_CTYPE, "polish_POLAND");
	printf("Prosz� poda� r�wnanie:\n");
}

