#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include "equation.h"
#include "tree.h"

char* GetEquation(void) {
	int i = 0, currentEqSize = 0;
	char* equation = NULL;
	char* equationTmp = NULL;

	do{
		equationTmp = realloc(equation, currentEqSize+=BASE_EQ_SIZE);
		if (equationTmp != NULL)
			equation = equationTmp;
		else
			printf("Error reallocating memory at line: %d", __LINE__);

		while (i < currentEqSize) {
			equation[i] = getchar();
			if (equation[i] == '\n') {
				equation[i] = '\0';
				break;
			}
			else
				i++;
		}
	} while (i == currentEqSize);

	return equation;
}

char* DelWhiteCharsAndUpperCase(char* equation) {
	int i, nOfWhiteChars = 0, old_nOfWhiteChars = 0;
	char* equationTmp = NULL;

	for (i = 0; equation[i] != '\0'; i++) {
		if (isspace((int)equation[i])) {
			strcpy_s(&equation[i], strlen(&equation[i+1])+1, &equation[i + 1]);
			nOfWhiteChars++;
			i--;
		}
		else if (equation[i] == 'p')
			equation[i] = 'P';
		else if (equation[i] == 's')
			equation[i] = 'S';
	}

	if (nOfWhiteChars) {
		equationTmp = malloc((i+1)*sizeof(char));
		if (equationTmp != NULL) {
			strcpy_s(equationTmp, strlen(equation)+1, equation);
			free(equation);
			return(equationTmp);
		}
		else
			printf("Error allocating memory at line: %d", __LINE__);
	}

	return equation;
}

int ParseEquation(char* equation) {
	int lBrackets = 0, rBrackets = 0, whichChar = 0;
	char curChar;
	int nextChar, theEnd = 0; 
	/* Nast�pnym znakiem musi by�: 
		1 ->	na pocz�tku lub po {+ - * / ( ,}	-> cyfra S P (
		2 -> 	po cyfrze							-> cyfra . + - * / ) , koniec
		3 -> 	po )								-> + - * / ) , koniec
		4 -> 	po S P								-> (
		5 ->	po .								-> cyfra
	*/
	nextChar = 1;
	while(!theEnd) {
		curChar = equation[whichChar];
		if (curChar == '(')
			lBrackets++;
		else if (curChar == ')')
			rBrackets++;

		switch (nextChar) {
		case 1:
			if ((curChar >= FIRST_ASCII_NUM && curChar <= LAST_ASCII_NUM) || curChar == 'P' || curChar == 'S' || curChar == '(')
				break;
			else
				return 1;
		case 2:
			if (curChar >= FIRST_ASCII + 1 && curChar <= LAST_ASCII)
				break;
			else if (curChar == '\0') {
				theEnd = 1;
				break;
			}
			else
				return 2;
		case 3:
			if (curChar >= FIRST_ASCII+1 && curChar <= '-' || curChar == '/')
				break;
			else if (curChar == '\0') {
				theEnd = 1;
				break;
			}
			else
				return 3;
		case 4:
			if (curChar == '(' && SpecialFunctionOK(equation, whichChar-1))
				break;
			else
				return 4;
		case 5:
			if (curChar >= FIRST_ASCII_NUM && curChar <= LAST_ASCII_NUM)
				break;
			else
				return 5;
		default:
			return 6;
		}

		nextChar = CheckNextChar(curChar);
		whichChar++;

		if (rBrackets > lBrackets)
			return 7;
	}

	if (lBrackets != rBrackets)
		return 8;

	whichChar = 0;
	theEnd = 0;
	while (equation[whichChar] != '\0') {
		curChar = equation[whichChar];
		if (curChar >= FIRST_ASCII_NUM && curChar <= LAST_ASCII_NUM) {
			if (GetNumberCheck(equation, &whichChar))
				return 8;
		}
		else if (curChar == '\\' && (curChar >= FIRST_ASCII_NUM && curChar <= LAST_ASCII_NUM)) {
			whichChar++;
			if (GetNumber(equation, &whichChar) == 0)
				return 9;
		}

		whichChar++;
	}

	return 0; //Powodzenie parsowania
}

int CheckNextChar(char curChar) {
	int nextChar;

	if (curChar == '+' || curChar == '-' || curChar == '/' || curChar == '*' || curChar == '(' || curChar == ',')
		nextChar = 1;
	else if (curChar >= FIRST_ASCII_NUM && curChar <= LAST_ASCII_NUM)
		nextChar = 2;
	else if (curChar == ')')
		nextChar = 3;
	else if (curChar == 'S' || curChar == 'P')
		nextChar = 4;
	else if (curChar == '.')
		nextChar = 5;
	else
		nextChar = 6;

	return nextChar;
}

int SpecialFunctionOK(char* equation, int symbolNum) {
	int maxCommas = (equation[symbolNum] == 'S' ? 0 : 1);
	int currentCommas = 0, lBrackets = 0, rBrackets = 0;
	char curChar;

	do {
		curChar = equation[++symbolNum];
		switch (curChar)
		{
		case '(':
			lBrackets++;
			break;
		case ')':
			rBrackets++;
			break;
		case ',':
			if (lBrackets - rBrackets == 1)
				currentCommas++;
		default:
			break;
		}
	} while (lBrackets != rBrackets && currentCommas <= maxCommas);

	if (currentCommas == maxCommas)
		return 1;
	else
		return 0;
}