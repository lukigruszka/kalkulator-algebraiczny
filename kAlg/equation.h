#ifndef EQUATION_H
#define EQUATION_H 1

#define BASE_EQ_SIZE 10
#define FIRST_ASCII 40 //(
#define FIRST_ASCII_NUM 48
#define LAST_ASCII_NUM 57
#define LAST_ASCII 57 //9

char* GetEquation(void);
char* DelWhiteCharsAndUpperCase(char* equation);
int ParseEquation(char* equation);
int	CheckNextChar(char curChar);
int SpecialFunctionOK(char* equation, int symbolNum);

#endif
