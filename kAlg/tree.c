#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "equation.h"
#include "tree.h"

int ID = 0;

Node* BuildTree(char* equation, int* curCharPos) {
	Node *root = NULL, *curOp = NULL, *newNode;
	short int lastNum = 0;
	int i = 0, lBrackets = 0, rBrackets = 0;
	double tmpNum = 0;
	char curChar;

	curChar = equation[*curCharPos];
	while(curChar != '\0' && curChar != ',') {
		if (curChar >= FIRST_ASCII_NUM && curChar <= LAST_ASCII_NUM) {
			tmpNum = GetNumber(equation, curCharPos);
			lastNum = 1;
		}
		else {
			if (curChar == '(' || curChar == 'P' || curChar == 'S')
				lBrackets++;
			else if (curChar == ')') {
				rBrackets++;
				if (rBrackets > lBrackets)
					break;
			}

			newNode = NewNode();
			if (root) {
				newNode->parentPtr = curOp;
				curOp->rChildPtr = newNode;
			}
			else
				root = newNode;
			curOp = newNode;
			curOp->ID = ID++;
			
			if (lastNum && curChar == ')') {
				curOp->operation = '=';
				curOp->result = tmpNum;
				lastNum = 0;

				newNode = NewNode();
				curOp->rChildPtr = newNode;
				curOp->rChildPtr->parentPtr = curOp;
				curOp = newNode;
				curOp->operation = curChar;
				curChar = equation[++(*curCharPos)];
				continue;
			}
			else 
				curOp->operation = curChar;

			if (curChar == 'P') {
				*curCharPos += 2;
				curOp->thirdDim = BuildTree(equation, curCharPos);
				curOp->thirdDim->parentPtr = curOp;

				(*curCharPos)++;
				curOp->fourthDim = BuildTree(equation, curCharPos);
				(*curCharPos)--;
				curOp->fourthDim->parentPtr = curOp;
				while(curOp->rChildPtr != NULL)
					curOp = curOp->rChildPtr;
			}
			else if (curChar == 'S') {
				*curCharPos += 2;
				curOp->thirdDim = BuildTree(equation, curCharPos);
				curOp->thirdDim->parentPtr = curOp;
			}
			else if (curChar == '*' || curChar == '/' || curChar == '+' || curChar == '-') {
				if (lastNum) {
					newNode = NewNode();
					newNode->operation = '=';
					newNode->result = tmpNum;
					curOp->lChildPtr = newNode;
					curOp->lChildPtr->parentPtr = curOp;
					lastNum = 0;
				}
			}
		}

		curChar = equation[++(*curCharPos)];
	}

	if (lastNum) {
		newNode = NewNode();
		newNode->operation = '=';
		newNode->result = tmpNum;
		if (root != NULL) {
			curOp->rChildPtr = newNode;
			curOp->rChildPtr->parentPtr = curOp;
		}
		else
			root = newNode;
	}

	return root;
}

void ReBuildTree(Node** root) {
	int nOfRotations = 0;

	Make3DTree(*root);
	do {
		nOfRotations = 0;
		Rotate(root, *root, &nOfRotations);
	} while (nOfRotations);
	Make2DTree(root, *root);
}

double SolveTree(Node* curNode) {
	double result;

	if (curNode->operation == '=')
		result = curNode->result;
	else if (curNode->operation == 'S')
		result = sqrt(SolveTree(curNode->lChildPtr));
	else if (curNode->operation == 'P')
		result = pow(SolveTree(curNode->lChildPtr), SolveTree(curNode->rChildPtr));
	else if (curNode->operation == '+')
		result = SolveTree(curNode->lChildPtr) + SolveTree(curNode->rChildPtr);
	else if (curNode->operation == '-')
		result = SolveTree(curNode->lChildPtr) - SolveTree(curNode->rChildPtr);
	else if (curNode->operation == '*')
		result = SolveTree(curNode->lChildPtr) * SolveTree(curNode->rChildPtr);
	else if (curNode->operation == '/')
		result = SolveTree(curNode->lChildPtr) / SolveTree(curNode->rChildPtr);

	free(curNode);
	return result;
}

void DisplayTree(Node* node) {
	int i;

	for (i = 0; i < GetLevel(node); i++)
		printf("\t");
	printf("%c", node->operation);
	if (node->operation == '=')
		printf("%g", node->result);
	printf("\n");
	if (node->rChildPtr != NULL)
		DisplayTree(node->rChildPtr);
	if (node->lChildPtr != NULL)
		DisplayTree(node->lChildPtr);
	if (node->thirdDim != NULL)
		DisplayTree(node->thirdDim);
	if (node->fourthDim != NULL)
		DisplayTree(node->fourthDim);
}

int GetLevel(Node* node) {
	Node* curOp = node;
	int level = 0;
	while (curOp->parentPtr != NULL) {
		curOp = curOp->parentPtr;
		level++;
	}
	return level;
}

void Rotate(Node** root, Node* curNode, int* nOfRotations) {
	if (curNode->lChildPtr != NULL) {
		if (GetPriority(curNode->lChildPtr) < GetPriority(curNode)) {
			if (curNode->parentPtr == NULL)
				*root = curNode->lChildPtr;
			RotateRight(curNode->lChildPtr);
			(*nOfRotations)++;
		}
		if (curNode->lChildPtr != NULL)
			Rotate(root, curNode->lChildPtr, nOfRotations);
	}
	if (curNode->rChildPtr != NULL) {
		if (GetPriority(curNode->rChildPtr) < GetPriority(curNode)) {
			if (curNode->parentPtr == NULL)
				*root = curNode->rChildPtr;
			RotateLeft(curNode->rChildPtr);
			(*nOfRotations)++;
		}
		if(curNode->rChildPtr != NULL)
			Rotate(root, curNode->rChildPtr, nOfRotations);
	}
	if (curNode->thirdDim != NULL)
		Rotate(root, curNode->thirdDim, nOfRotations);
	if (curNode->fourthDim != NULL)
		Rotate(root, curNode->fourthDim, nOfRotations);
}

void RotateRight(Node* pivot) {
	Node* root = pivot->parentPtr;

	pivot->parentPtr = root->parentPtr;
	if (root->parentPtr != NULL) {
		if (root->parentPtr->rChildPtr == root)
			root->parentPtr->rChildPtr = pivot;
		else if (root->parentPtr->lChildPtr == root)
			root->parentPtr->lChildPtr = pivot;
		else if (root->parentPtr->thirdDim == root)
			root->parentPtr->thirdDim = pivot;
		else if (root->parentPtr->fourthDim == root)
			root->parentPtr->fourthDim = pivot;
	}
	root->lChildPtr = pivot->rChildPtr;
	if (pivot->rChildPtr != NULL)
		pivot->rChildPtr->parentPtr = root;
	pivot->rChildPtr = root;
	root->parentPtr = pivot;
}

void RotateLeft(Node* pivot) {
	Node* root = pivot->parentPtr;

	pivot->parentPtr = root->parentPtr;
	if (root->parentPtr != NULL) {
		if (root->parentPtr->rChildPtr == root)
			root->parentPtr->rChildPtr = pivot;
		else if(root->parentPtr->lChildPtr == root)
			root->parentPtr->lChildPtr = pivot;
		else if(root->parentPtr->thirdDim == root)
			root->parentPtr->thirdDim = pivot;
		else if (root->parentPtr->fourthDim == root)
			root->parentPtr->fourthDim = pivot;
	}
	root->rChildPtr = pivot->lChildPtr;
	if(pivot->lChildPtr != NULL)
		pivot->lChildPtr->parentPtr = root;
	pivot->lChildPtr = root;	
	root->parentPtr = pivot;
}

int GetPriority(Node* node) {
	int priority;
	char operation = node->operation;

	if (operation == '+' || operation == '-')
		priority = 0;
	else if (operation == '/' || operation == '*')
		priority = 1;
	else if (operation == '(' || operation == 'P' || operation == 'S' || operation == '=')
		priority = 2;
	else
		priority = -1;
	
	return priority;
}

void Make3DTree(Node* curNode){
	Node* tmp;
	
	tmp = curNode->rChildPtr;
	if (tmp != NULL && (curNode->operation == '(' || curNode->operation == 'P')) {
		while (tmp->operation != ')') {
			if (tmp->operation == '(' || tmp->operation == 'P')
				Make3DTree(tmp);
			tmp = tmp->rChildPtr;
		}
		if(curNode->operation == '(')
			curNode->thirdDim = curNode->rChildPtr;
		curNode->rChildPtr = tmp->rChildPtr;
		if (curNode->rChildPtr != NULL)
			curNode->rChildPtr->parentPtr = curNode;
		if(tmp->parentPtr != curNode)
			tmp->parentPtr->rChildPtr = NULL;
		free(tmp);
	}
	/*else if (curNode->operation == ')') {
		if (curNode->parentPtr->lChildPtr == curNode) {
			tmp = curNode;
			curNode->parentPtr->lChildPtr = curNode->rChildPtr;
			if (curNode->rChildPtr != NULL)
				curNode->rChildPtr->parentPtr = curNode->parentPtr;
			curNode = curNode->parentPtr;
			free(tmp);
		}
		else {
			tmp = curNode;
			curNode->parentPtr->rChildPtr = curNode->rChildPtr;
			if (curNode->rChildPtr != NULL)
				curNode->rChildPtr->parentPtr = curNode->parentPtr;
			curNode = curNode->parentPtr;
			free(tmp);
		}
	}*/
	if (curNode->operation != '(') {
		if (curNode->thirdDim != NULL)
			Make3DTree(curNode->thirdDim);
		}
		if (curNode->rChildPtr != NULL)
			Make3DTree(curNode->rChildPtr);
		if (curNode->lChildPtr != NULL)
			Make3DTree(curNode->lChildPtr);
		if (curNode->fourthDim != NULL)
			Make3DTree(curNode->fourthDim);
}

void Make2DTree(Node** root, Node* curNode) {
	Node* tmp;
	if (curNode->operation == '(' || curNode->operation == 'P' || curNode->operation == 'S') {
		curNode->lChildPtr = curNode->thirdDim;
		curNode->thirdDim = NULL;
		curNode->rChildPtr = curNode->fourthDim;
		curNode->fourthDim = NULL;
	}
	if (curNode->operation == '(') {
		tmp = curNode;
		if (curNode->parentPtr == NULL)
			*root = curNode->lChildPtr;
		else {
			if (curNode->parentPtr->rChildPtr == curNode)
				curNode->parentPtr->rChildPtr = curNode->lChildPtr;
			else
				curNode->parentPtr->lChildPtr = curNode->lChildPtr;
		}
		curNode->lChildPtr->parentPtr = curNode->parentPtr;
		curNode = curNode->lChildPtr;
		free(tmp);
	}

	if (curNode->rChildPtr != NULL)
		Make2DTree(root, curNode->rChildPtr);
	if (curNode->lChildPtr != NULL)
		Make2DTree(root, curNode->lChildPtr);
}

Node* NewNode(void) {
	Node* newNode;
	newNode = calloc(1, sizeof(Node));
	if(!newNode)
		printf("Error allocating memory at line: %d", __LINE__);
	else {
		newNode->lChildPtr = NULL;
		newNode->rChildPtr = NULL;
		newNode->parentPtr = NULL;
	}

	return newNode;
}

short int GetNumberCheck(char* equation, int* curCharPos) {
	short int comas = 0;
	char curChar;

	curChar = equation[*curCharPos];
	while (comas <= 1) {
		if (curChar == '.')
			comas++;
		else if(!(curChar >= FIRST_ASCII_NUM && curChar <= LAST_ASCII_NUM))
			break;

		(*curCharPos)++;
		curChar = equation[*curCharPos];
	}

	if(comas > 1)
		return 1;

	(*curCharPos)--;
	return 0;
}

double GetNumber(char* equation, int* curCharPos) {
	double number;
	char charTab[64], curChar;
	short int i=0;

	while (1) {
		curChar = equation[*curCharPos];
		if (curChar == '.' || (curChar >= FIRST_ASCII_NUM && curChar <= LAST_ASCII_NUM))
			charTab[i++] = curChar;
		else
			break;
		(*curCharPos)++;
	}

	(*curCharPos)--;
	charTab[i] = '\0';
	number = atof(charTab);
	return number;
}