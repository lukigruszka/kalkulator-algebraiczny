#ifndef TREE_H
#define TREE_H 1

typedef struct NODE {
	int ID;
	double result;
	char operation;

	struct NODE *parentPtr, *lChildPtr, *rChildPtr, *thirdDim, *fourthDim;

}Node;

Node* BuildTree(char* equation, int* curCharPos);
void ReBuildTree(Node**);
double SolveTree(Node* tree);
void DisplayTree(Node*);

void Rotate(Node** root, Node* curNode, int* nOfRotations);
void RotateRight(Node* pivot);
void RotateLeft(Node* pivot);
int GetPriority(Node* node);
void Make3DTree(Node* root);
void Make2DTree(Node** root, Node* curNode);
int GetLevel(Node* node);
Node* NewNode(void);
short int GetNumberCheck(char* equation, int* curCharPos);
double GetNumber(char* equation, int* curCharPos);

#endif